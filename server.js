'use strict';

import HID from 'node-hid';

console.log('Searching for device...');

const devices = HID.devices();
const deviceInfo = devices.find(d => (
    d.vendorId === 0x16c0 &&
    d.productId === 0x05df &&
    d.manufacturer === 'usb@6bez10.info' &&
    d.product === 'IR Tx v1' &&
    d.usagePage === 0x000c &&
    d.usage === 0x105
));

if (!deviceInfo) {
    console.error('No compatible device found!');
    process.exit(1);
}

console.log('Device found!');

const device = new HID.HID(deviceInfo.path);

console.log('Device connected!');

const on = true;
const mode = 0; // HEATING
const temperature = 24;
const fan = 0;
const minutes = 0;
const hours = 0;
const dimmer = true;

let byte2, byte17;

switch (fan) {
    case 0:
        byte2 = byte17 = 0;
        break;
    case 1:
        byte2 = 3;
        byte17 = 0;
        break;
    case 2:
        byte2 = 3;
        byte17 = 0x40;
        break;
    case 3:
        byte2 = 2;
        byte17 = 0;
        break;
    case 4:
        byte2 = 1;
        byte17 = 0x40;
        break;
    case 5:
        byte2 = 1;
        byte17 = 0;
        break;
}

const byte3 = (temperature - 16) << 4 | mode;

const byte6 = hours | (dimmer ? 0x20 : 0);
const byte7 = minutes;
const byte15 = dimmer ? 0 : 0x02;

const byte18 = on ? 0x38 : 0x28;

const checksum = arr => arr.reduce((prev, curr) => prev ^ curr, 0);

let report = [
    0x00, // Report ID (0)
    0x01, // Command (Send IR) (1)
];

const TIMER_VAL_USEC = (PS, usec) => Math.round(16500000 / PS / (1e6 / usec));

report.push(0b01000101);
report.push(TIMER_VAL_USEC(1024, 9000));

report.push(0b00000101);
report.push(TIMER_VAL_USEC(1024, 4500));

report.push(0b10000100);
report.push(TIMER_VAL_USEC(256, 550));
report.push(TIMER_VAL_USEC(256, 550));
report.push(TIMER_VAL_USEC(256, 1700));
report.push(6);

report.push(0x83);
report.push(0x06);
const idx1 = report.length;
report.push(byte2);
report.push(byte3);
report.push(0);
report.push(0);

report.push(0b01000101);
report.push(TIMER_VAL_USEC(1024, 550));

report.push(0b00000101);
report.push(TIMER_VAL_USEC(1024, 8000));

report.push(0b10000100);
report.push(TIMER_VAL_USEC(256, 550));
report.push(TIMER_VAL_USEC(256, 550));
report.push(TIMER_VAL_USEC(256, 1700));
report.push(8);

const idx2 = report.length;
report.push(byte6);
report.push(byte7);
report.push(0);
report.push(0);
report.push(0);
report.push(0);
report.push(0);
report.push(checksum(report.slice(idx1, idx1 + 4).concat(report.slice(idx2, idx2 + 7))));

report.push(0b01000101);
report.push(TIMER_VAL_USEC(1024, 550));

report.push(0b00000101);
report.push(TIMER_VAL_USEC(1024, 8000));

report.push(0b10000100);
report.push(TIMER_VAL_USEC(256, 550));
report.push(TIMER_VAL_USEC(256, 550));
report.push(TIMER_VAL_USEC(256, 1700));
report.push(7);

const idx3 = report.length;
report.push(0);
report.push(byte15);
report.push(0);
report.push(byte17);
report.push(byte18);
report.push(0);
report.push(checksum(report.slice(idx3, idx3 + 6)));

if (report.length > 64) {
    console.error('Command too long!');
    process.exit(1);
}

console.log('Command length: ', report.length, ' bytes');
console.log('Sending command...');

const bytesWritten = device.sendFeatureReport(report);

if (report.length !== bytesWritten) {
    console.error('Unexpected send bytes count, expected ', report.length, ', got ', bytesWritten);
    process.exit(1);
}

console.log('Command sent successfully!');
